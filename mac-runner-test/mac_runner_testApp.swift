//
//  mac_runner_testApp.swift
//  mac-runner-test
//
//  Created by Chan Ko on 14/2/2024.
//

import SwiftUI

@main
struct mac_runner_testApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
